const express = require("express");
const jwt = require("jsonwebtoken");

const app = express();
//Add ruta
app.get("/api", (req, res) => {
    res.json({
        mensaje: "Nodejs y JWT"
    });

});
//Add ruta 2
app.post("/api/login", (req, res) => {
    //Información del usuario
    const user = {
        id: 1,
        nombre: "Coraima",
        email: "coraima@gmail.com"
    }
    //Crear Token
    jwt.sign({ user }, 'secretkey',{expiresIn:'32s'}, (err, token) => {
        res.json({
            token
        });

    });

});
//Add ruta 3
app.post("/api/posts", verificarToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (error, authData) => {
        if(error){
            res.sendStatus(403);
        }else{  
            res.json({
                mensaje: "POST fue creado",
                authData
            })
        }
    });
});
//Authorization: Bearer <token>
 function verificarToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    }else{ 
        res.sendStatus(403);

    }
 }

app.listen(3000, function () {
    console.log("App running...");
});