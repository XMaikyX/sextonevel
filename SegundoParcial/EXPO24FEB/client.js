import routes from './routes.jsx';

import Client from 'react-engine/lib/client';

document.addEventListener('DOMContentLoaded', function onLoad() {

  Client.boot({ routes });
});