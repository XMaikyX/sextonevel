import React from 'react';
import Router from 'react-router';
// componente   

import Home from './Home';

const routes = (
  <Router.Route path='/' handler={ App }>
  <Router.DefaultRoute name='home' handler={ Home } />
  </Router.Route>
);

export default routes;