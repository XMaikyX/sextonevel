require ('babel-register')({
    presets:['react']
});

const express = require ('express');
const React = require('react')
const ReactDomServer = require('react-dom/server')

const app=express();
const Component = require('./Home.jsx')

app.get('/',(req,res)=>{
    var html =ReactDomServer.renderToString(
        React.createElement(Component)
    );
    res.send(html)
})

app.listen(8080,()=>{
    console.log("El servidor esta activo en el puerto 8080")
})