import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "react-router-dom";
import Bebidas from "./paginas/Bebidas";
import Comidas from "./paginas/Comidas";
import Combos from "./paginas/Combos";
function App(){
  return (
    <Router>
      <div className="container"> 
      <center><h1>Locos Por La Comida</h1></center>
      <hr/>
      <Switch>
        <Route path="/bebidas">
          <Bebidas/>
        </Route>
        <Route path="/" exact>
          <Comidas />
        </Route>

        <Route path="/comidas">
          <Comidas />
        </Route>
        <Route path="/combos">
          <Combos/>
        </Route>

      </Switch>
      </div>

    </Router>
  );
}
export default App;