const express = require('express');
const app = express();

//Definir db
const response =
{
    data: [],
    services: "Users services",
    architecture: "Microservices"
}
//funcion fleca-mensaje
const logger = message => console.log(`Mensaje desde Users service: ${message}`);

app.use((req, res, next) => {
    response.data = [];
    next();
})
//http://localhost:5000/api/v2/users
app.get("/api/v2/users", (req, res) => {
    response.data.push(
        "Admin",
        "Root",
        "Guest"
    );
    //Pasar mensaje
    logger("Get data beverages");
    return res.send(response);
})
module.exports = app;