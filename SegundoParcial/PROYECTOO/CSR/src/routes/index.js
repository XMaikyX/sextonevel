const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
require('express-async-errors');

module.exports= function({ HomeRoutes, UserRoutes }){
    const router = express.Router();
    const apiRouter = express.Router();

    apiRouter.use(express.json())
    .use(cors())
    .use(helmet())
    .use(compression())
    

    apiRouter.use('/home', HomeRoutes);
    apiRouter.use('/user', UserRoutes);


    router.use('/v1/api', apiRouter );
    
    //ruta estAtica
    router.use('/public', express.static('public'));
    



    return router;




}


