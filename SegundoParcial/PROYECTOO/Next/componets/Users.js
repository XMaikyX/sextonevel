
const Users = (props) =>{
    return( 
        <ul>
            {
            props.users.map(user=>(
                <li className="list-group-item list-group">
                    <h5>{user.name}</h5>
                </li>
            ))
        }
        </ul>
        
    )
}

export default Users;