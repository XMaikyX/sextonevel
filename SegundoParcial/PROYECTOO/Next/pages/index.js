import Head from 'next/head'
import Container from "../componets/container"
import fetch from 'isomorphic-fetch'
import Users from '../componets/Users'

const Index = (props)=>{
    console.log(props)
    return (
        <Container>
            <Head>
                <title> Next.js Proyect - Home</title>
            </Head>
            <h1>Next</h1>
            <Users users={props.users} />
        </Container>
    );
};
Index.getInitialProps = async (ctx) =>{
  const res= await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await res.json();
  return {users:data}
}
export default Index;