let mongoose = require('mongoose');
let Schema = mongoose.Schema;


// se define el esquema o la estructura de la entidad
let FoodSchema = new Schema({
    name:{type:String, required:true},
    foodname:{ type:String, required:true},
    tipo:{type:String, required:true}
});


//se crea el modelo a partir del esquema
let Comida = mongoose.model('Comidas', FoodSchema);

module.exports= Comida;
