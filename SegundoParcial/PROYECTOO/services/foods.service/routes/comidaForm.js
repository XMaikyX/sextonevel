let express = require('express');
let router = express.Router();

let mongoose = require('./../config/conexion');
let Comida = require('./../models/food')



router.post('/comida/operar', (req,res,next)=>{
    //if _id esta vacio si creo uno nuevo
    if (req.body._id === "")
    {
        let bocado = new Comida({
            name:req.body.name,
            foodname: req.body.foodname,
            tipo: req.body.tipo
        });
        bocado.save();
    }
    //caso contrario modificamos
    else
    {
        Comida.findByIdAndUpdate(req.body._id, {$set:req.body }, {new:true}, (err,model)=>{
            if (err) throw err;
        })
    }

    res.redirect('/comida');

})


module.exports= router;
