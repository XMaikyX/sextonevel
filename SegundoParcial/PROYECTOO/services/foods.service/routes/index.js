//servidor express
var express = require('express');
var router = express.Router();


//conexiOn a la base de datos
let mongoose = require('./../config/conexion');

//modelos
let Comida = require('../models/food');



//ruta /

router.get('/comida', (req,res, next)=>{
  Comida.find((err, comidas)=>{
    if (err) throw err;
    res.render('index', {comidas: comidas});
  })
})
//ruta usuario/nuevo
router.get('/comida/nuevo',(req,res,next)=>{
  res.render('comidaForm', {});
})
//ruta usuario/modificar
router.get('/comida/modificar/:id', (req,res,next)=>{
  let idComida= req.params.id;
  Comida.findOne({_id: idComida }, (err,comida)=>{
    if (err) throw err;
    res.render('comidaForm', {comida: comida});
  })
})
//ruta usuario/eliminar
router.get('/comida/eliminar/:id', (req,res,next)=>{
  let idComida= req.params.id;
  Comida.remove({_id:idComida}, (err)=>{
    if (err) throw err;
    res.redirect('/comida');
  })
})



module.exports= router;
