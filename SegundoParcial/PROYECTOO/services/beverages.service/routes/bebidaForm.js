let express = require('express');
let router = express.Router();

let mongoose = require('./../config/conexion');
let Bebida = require('../models/beverage')



router.post('/bebida/operar', (req,res,next)=>{
    //if _id esta vacio si creo uno nuevo
    if (req.body._id === "")
    {
        let liquido= new Bebida({
            name:req.body.name,
            beveragename: req.body.beveragename,
            tipo: req.body.tipo
        });
        liquido.save();
    }
    //caso contrario modificamos
    else
    {
        Bebida.findByIdAndUpdate(req.body._id, {$set:req.body }, {new:true}, (err,model)=>{
            if (err) throw err;
        })
    }

    res.redirect('/');

})


module.exports= router;
