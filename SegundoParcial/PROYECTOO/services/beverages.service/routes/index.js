//servidor express
var express = require('express');
var router = express.Router();


//conexiOn a la base de datos
let mongoose = require('./../config/conexion');

//modelos
let Bebida = require('../models/beverage');



//ruta /

router.get('/bebida', (req,res, next)=>{
  Bebida.find((err, bebidas)=>{
    if (err) throw err;
    res.render('index', {bebidas: bebidas});
  })
})
//ruta bebida/nuevo
router.get('/bebida/nuevo',(req,res,next)=>{
  res.render('bebidaForm', {});
})
//ruta bebida/modificar
router.get('/bebida/modificar/:id', (req,res,next)=>{
  let idBebida= req.params.id;
  Bebida.findOne({_id: idBebida }, (err,bebida)=>{
    if (err) throw err;
    res.render('bebidaForm', {bebida: bebida});
  })
})
//ruta bebida/eliminar
router.get('/bebida/eliminar/:id', (req,res,next)=>{
  let idBebida= req.params.id;
  Bebida.remove({_id:idBebida}, (err)=>{
    if (err) throw err;
    res.redirect('/bebida');
  })
})



module.exports= router;
