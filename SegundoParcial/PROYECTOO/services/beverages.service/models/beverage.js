let mongoose = require('mongoose');
let Schema = mongoose.Schema;


// se define el esquema o la estructura de la entidad
let BeverageSchema = new Schema({
    name:{type:String, required:true},
    beveragename:{ type:String, required:true},
    tipo:{type:String, required:true}
});


//se crea el modelo a partir del esquema
let Bebida = mongoose.model('Bebidas', BeverageSchema);

module.exports= Bebida;
