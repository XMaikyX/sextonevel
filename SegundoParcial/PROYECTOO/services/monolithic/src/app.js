//1crear rutas express
const express = require('express');
const app = express();
//Arr
const response =
{
    data: [],
    services: "Monolithic service",
    architecture: "Monolithic",
}
//Definir sercio monolitico-rutas

app.use((req,res,next)=>{
    response.data=[];
    next();
})
app.get('/api/v3/user', (req,res)=>{
    response.data.push(
        'Administrador',
        'Invitado',
        'Coraima'
        );
    return res.send(response)
})
app.get('/api/v3/beverages',(req,res)=>{
    response.data.push(
        'Colas',
        'Batidos',
        'Té'
        );
    return res.send(response)
})

app.get('/api/v3/foods',(req,res)=>{
    response.data.push(
        'Hamburguesas',
        'Salchipapas',
        'Hot Dog'
        );
    return res.send(response)
})

module.exports = app;