let mongoose = require('mongoose');
let Schema = mongoose.Schema;


// se define el esquema o la estructura de la entidad
let UserSchema = new Schema({
    name:{type:String, required:true},
    username:{ type:String, required:true},
    password:{type:String, required:true}
});


//se crea el modelo a partir del esquema
let Usuario = mongoose.model('Usuarios', UserSchema);

module.exports= Usuario;
