var URLRest= `http://localhost:5000/v1/api/user`

window.addEventListener('load', function(){
    let htmlGenerado=''; 


    htmlGenerado+=`<label for="txtid">ID</label>`;
    htmlGenerado+=`<input type="text" id="txtid">`;
    htmlGenerado+=`<label for="txtname">Nombre</label>`;
    htmlGenerado+=`<input type="text" id="txtname">`;
    htmlGenerado+=`<label for="txtusername">Nombre de Usuario</label>`;
    htmlGenerado+=`<input type="text" id="txtusername">`;
    htmlGenerado+=`<label for="txtpassword">Contraseña</label>`;
    htmlGenerado+=`<input type="text" id="txtpassword">`;
    htmlGenerado+=`<button id="btnnuevo">Nuevo</button>`;
    htmlGenerado+=`<button id="btnconsultar">Consultar</button>`;
    htmlGenerado+=`<button id="btngrabar">Grabar</button>`;
    htmlGenerado+=`<button id="btnmodificar">Modificar</button>`;
    htmlGenerado+=`<button id="btneliminar">Eliminar</button>`;
    htmlGenerado+=`<div id="divcontenido"></div>`;

    htmlCuerpo.innerHTML=htmlGenerado;

    //funcionabilidades
    //nuevo
    btnnuevo.addEventListener('click',function(){
        txtid.value='';
        txtname.value='';
        txtusername.value='';
        txtpassword.value=''
    });
    
    //consultar
    btnconsultar.addEventListener('click',function(){
        fetch(URLRest).then(resultado=>{
            return resultado.json()
        })
        .then(consulta=>{
            var htmlGenerado=`<table border=1>`
            for (const indice in consulta)
            {
                htmlGenerado+=`<tr>`
                let elemento = consulta[indice];
                htmlGenerado+=`<td>${elemento.name}</td><td>${elemento.password}</td><td><button class='consultar' value='${elemento._id}'>${elemento.username}</button></td>` 
                htmlGenerado+=`</tr>`
            }
            //insertamos el html al div
            divcontenido.innerHTML=htmlGenerado
            //asignaciOn a consulta indidividual
            document.querySelectorAll(`.consultar`).forEach(elemento=>{
                elemento.addEventListener('click', function(){
                    consultaIndividual(elemento.value);
                })
            })

        })
    })
    //grabar
    btngrabar.addEventListener('click',function(){
        var data = {name:txtname.value, username: txtusername.value  , password: txtpassword.value };
        fetch(
            URLRest,
            {
                method:'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-type':'application/json'
                }
            }
        ).then(respuesta=>{
            return respuesta.json()
        })
        .then(resultado=>{
            console.log(`El usuario con nombre ${resultado.name} y el ID ${resultado._id} se almacenó correctamente`)
        })
        .catch(error=>{
            console.log(`Error`, error)
        })

    })
    //consulta individual
    function consultaIndividual(parametro)
    {
        var url = `${URLRest}/${parametro}`
        fetch(url).then(resultado=>{
            return resultado.json();
        }).then(respuesta=>{
            txtid.value= respuesta._id;
            txtname.value= respuesta.name;
            txtusername.value= respuesta.username;
            txtpassword.value= respuesta.password;

        })
    }
    
    //modificar
    btnmodificar.addEventListener('click',function(){
        var data = {name:txtname.value, username: txtusername.value  , password: txtpassword.value };
        fetch(
            `${URLRest}/${txtid.value}` ,
            {
                method:'PATCH',
                body: JSON.stringify(data),
                headers:{
                    'Content-type':'application/json'
                }
            }
        ).then(respuesta=>{
            return respuesta.json()
        })
        .then(resultado=>{
            console.log(`El usuario con nombre ${resultado.name} y el ID ${resultado._id} se almacenO correctamente`)
        })
        .catch(error=>{
            console.log(`Error`, error)
        })
    })
    //eliminar
    btneliminar.addEventListener('click',function(){
        var url = `${URLRest}/${txtid.value}`
        fetch(url, {
            method:'DELETE'
        }).then(respuesta=>{
            return respuesta.json()
        })
        .then(resultado=>{
            console.log(resultado)
        })
        .catch(error=>{
            console.log(`Error`, error)
        })
    })



})


    