const { createContainer, asClass, asFunction, asValue } = require('awilix');

// config
const config = require('../config');
//startup
const app =  require('.')

//models

const { User } = require('../models')

//repositorys

const { UserRepository } = require('../repositories')


//services

const { HomeService } = require('../services')
const { UserService} = require('../services')

//controllers

const { HomeController } = require('../controllers')
const { UserController } = require('../controllers')

//routes

const { HomeRoutes} = require('../routes/index.routes');
const { UserRoutes} = require('../routes/index.routes');
const Routes =  require('../routes')




const container = createContainer();


container.register(
    {
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config)
    }
).register(
    {
        HomeService: asClass(HomeService).singleton(),
        UserService: asClass(UserService).singleton()
    }
).register(
    {
        HomeController: asClass(HomeController.bind(HomeController)).singleton(),
        UserController: asClass(UserController.bind(UserController)).singleton()
    }
).register(
    {
        HomeRoutes: asFunction(HomeRoutes).singleton(),
        UserRoutes: asFunction(UserRoutes).singleton() 
    }
).register(
    {
        User: asValue(User)
    }
).register(
    {
        UserRepository: asClass(UserRepository).singleton()
    }
)


module.exports = container;
