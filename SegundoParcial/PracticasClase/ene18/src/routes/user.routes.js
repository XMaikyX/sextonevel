const{ Router } = require('express');

module.exports = function({UserController})
{
    const router= Router();
    router.get('/:userId', UserController.get);
    router.get('/', UserController.getAll);
    router.post('/', UserController.create);//recibe Body
    router.patch('/:userId', UserController.update); //recibe Body
    router.delete('/:userId', UserController.delete);
    return router;  
    
}