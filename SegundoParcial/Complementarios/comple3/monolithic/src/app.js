//1crear rutas express
const express = require('express');
const app = express();
//Arr
const response =
{
    data: [],
    services: "Monolithic service",
    architecture: "Monolithic",
}
//Definir sercio monolitico-rutas

//get-  api/v1 
app.use((req,res,next)=>{
    response.data=[];
    next();
})
//get usuarios- http://localhost:3000/api/v1/users
app.get('/api/v1/users', (req,res)=>{
    response.data.push(
        'Administrador',
        'Invitado',
        'Coraima'
        );
    return res.send(response)
})
//get bebidas- api/v1/beverages
app.get('/api/v1/beverages',(req,res)=>{
    response.data.push(
        'Colas',
        'Batidos',
        'Té'
        );
    return res.send(response)
})
//get comidas- api/v1/foods
app.get('/api/v1/foods',(req,res)=>{
    response.data.push(
        'Hamburguesas',
        'Salchipapas',
        'Hot Dog'
        );
    return res.send(response)
})

module.exports = app;