const express = require('express');
const app = express();

//Definir db
const response =
{
    data: [],
    services: "Foods services",
    architecture: "Microservices"
}
//funcion fleca-mensaje
const logger = message => console.log(`Mensaje desde Foods service: ${message}`);

app.use((req, res, next) => {
    response.data = [];
    next();
})
//http://localhost:5000/api/v2/foods
app.get("/api/v2/foods", (req, res) => {
    response.data.push(
        "Hamburguesas",
        "Salchipapas",
        "Hot Dog"
    );
    //Pasar mensaje
    logger("Get data Foods");
    return res.send(response);
})
module.exports = app;