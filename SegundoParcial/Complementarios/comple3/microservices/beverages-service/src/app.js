const express = require('express');
const app = express();

//Definir db
const response =
{
    data: [],
    services: "Beverages services",
    architecture: "Microservices"
}
//funcion fleca-mensaje
const logger = message => console.log(`Mensaje desde beverages service: ${message}`);

app.use((req, res, next) => {
    response.data = [];
    next();
})
//http://localhost:5000/api/v2/beverages
app.get("/api/v2/beverages", (req, res) => {
    response.data.push(
        "Colas",
        "Batidos",
        "Té"
    );
    //Pasar mensaje
    logger("Get data beverages");
    return res.send(response);
})
module.exports = app;