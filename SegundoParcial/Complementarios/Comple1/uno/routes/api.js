const express= require('express');
const router=express.Router();
const Product=require("../models/Products")

//
router.get("/chatbot",(req,res)=>{
    res.json({ok:true,msg:"Esto esta funcionando bien"});
});

router.post("/products",(req,res)=>{
    //variable para usar
    let body = req.body;

    let product= new Product({
        name:body.name,
        descripcion:body.descripcion,
        price:body.price,
        img:body.img,
    });
    product.save((err,productDB)=>{
        if(err) 
        return res.json(
            {
                ok:false,
                msg:"hubo un error"
            });
        res.json(
            {
                ok:true,
                msg:"Producto creado correctamente",
                product: productDB,
        });
    });
});

module.exports=router;
