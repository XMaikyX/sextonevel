const express = require("express");
const bodyParser = require("body-parser");
const mongoose= require('mongoose');
const app = express();

const port = process.env.PORT || 3000;

// for parsing json
app.use(
  bodyParser.json({
    limit: "20mb",
  })
);
// parse application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
    limit: "20mb",
  })
);
//conectar con mongoose
mongoose.connect('mongodb+srv://sextonivel:sexto123@cluster0.1wehz.mongodb.net/baseatlas?retryWrites=true&w=majority', 
{
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
},(error,res)=>{
  if(err)
  return console.log("Hay un error en la base de datos",error);
  console.log("Base de datos Conectado");
}
);

app.use("/messenger", require("./Facebook/facebookBot"));
app.use("/api", require("./routes/api"));


app.get("/", (req, res) => {
  return res.send("Chatbot Funcionando correctamente");
});

app.listen(port, () => {
  console.log(`Escuchando peticiones en el puerto ${port}`);
});
