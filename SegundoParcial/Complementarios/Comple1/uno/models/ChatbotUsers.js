const mongoose= require("mongoose");
const Schema = mongoose.Schema;
//const ObjectId = Schema.ObjectId;

const ChatbotUserSchema = new Schema(
    {
        firstName:String,
        LastName:String,
        facebookId:
        {
            type:String,
            unique:true
        },
        profilePic:String,
    },
    {timestamps:true}
);

module.exports=mongoose.model("ChatbotUser", ChatbotUserSchema);