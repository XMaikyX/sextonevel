const mongoose= require("mongoose");
const Schema = mongoose.Schema;
//const ObjectId = Schema.ObjectId;

const ProductSchema = new Schema(
    {
        name:{
            type:String,
            required:true,
            unique:true,
        },
        descripcion:String,
        price:Number,
        img:String,
    },
    {timestamps:true}
);

module.exports=mongoose.model("Products", ProductSchema);