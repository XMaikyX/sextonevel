const mongoose = require('mongoose');

//trabajar con esquema de mongoose
const {Schema}= mongoose

const UsuariosSchema= new Schema(
    {
        nombre:{type:String},
        apellido:{type:String}
    },
    { timestamps:{ createdAt: true, updatedAt:true}}
)

module.exports = mongoose.model("Usuarios", UsuariosSchema);
