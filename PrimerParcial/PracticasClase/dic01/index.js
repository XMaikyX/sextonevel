const mongoose = require('mongoose');
//librerias instaladas
const axios= require ('axios').default;
const cheerio = require('cheerio')
const cron= require('node-cron')
//Componentes Locales
const {MONGO_URI}= require ('./config')
const {Noticias} = require ('./models')


//conectarnos a nuestra base de datos ORM
mongoose.connect(MONGO_URI, {useNewUrlParser:true, useUnifiedTopology:true});
/*.then(p=>{
    console.log('conexion exitosa')
}).catch(err=>{
    console.log(err)
})*/
// definir la  peridiosidad utilizando CRON Expression
cron.schedule("* * * * * *", async ()=>{

    //acceder a nuestra URL de CNN para tomar el html con AXIOS
    const html = await axios.get("https://cnnespanol.cnn.com/");


    //procesar  ese HTML  utilizando  cheerio y recorrer las noticias
    const $= cheerio.load(html.data);

    
    //llamar a los parametros que pertenecen a la clase  new_title
    const titulos = $(".news__title");
    titulos.each((index,element)=>{

        //CONSULTAR LOS ELEMENTOS DE TIPO STRING PARA MOSTRAR LOS TITULOS
        //console.log($(element).text().toString())
    
        // almacenar la informacion en mongodb utilizando mongooze
        const noticia = {
            titulo: $(element).text().toString(),
            enlace: $(element).children().attr("href")
        }
        Noticias.create([noticia]);
    })

    console.log('proceso terminado')
})
