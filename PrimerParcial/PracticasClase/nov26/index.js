//definir una persistencia(Arreglo, base de datos, servicios web monitoreo)

const libros =[
    {
        id:1,
        titulo:'AMerican Pie',
        idautor:2
    },
    {
        id:2,
        titulo:'El socio',
        idautor:2
    },
    {
        id:3,
        titulo:' 30 dias de muerte',
        idautor:2
    }
]

const autores =[
    {
        id:1,
        nombre:'El pepe'
    },
    {
        id:2,
        nombre:' Stalone'
    },
    {
        id:3,
        nombre:'Luisillo Pillo'
    }
]



//funcion buscar libro ID
async function buscarLibro(id)
{
    // se busca en el arreglo que tenga un id
    const libro=libros.find(
        (libro)=>
        {   
            return libro.id=== id;
        }
    )
    // caso contrario envia error y lo lanzamos
    if ( !libro)
    {
        const error =new Error();
        error.message="No hay esa salvajada"
        throw error;
    }
    //si no existe libro lo regresamos
    return libro;

}

//buscar autor id
async function buscarAutor(id)
{
        
    // buscamos en el arreglo de autor con un id en especifico
    const autor=autores.find(
        (autor)=>{
            return autor.id=== id;
        }
    )
    // caso contrario envia error regresando al callback
    if ( !autor)
    {
        const error =new Error();
        error.message="No hay ese emprendedor"
        throw error;
    }
    //si no existe error debemos enviar el autor al callback
    return autor;

}

async function mainx()
{
    try
    {
        //busqueda de id de libro
        const libro = await buscarLibro(3);
        //empieza a buscar su respectivo autor
        const autor = await buscarAutor(libro.idautor);
        libro.autor=autor;
        delete libro.idautor;
        console.log(libro)
        //console.log(`El libro ${libro.titulo} tiene como autor A ${autor.nombre}`);
    }
    catch(second)
    {
        console.log(second.message);
    }
}

    
mainx();