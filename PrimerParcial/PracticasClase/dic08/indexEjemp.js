const fs = require('fs');
const express = require('express');
const path= require('path')

const puerto= 3000;
const index = fs.readFileSync('./index.html')
const about = fs.readFileSync('./about.html')

const server = express();

var paginaError= path.join( __dirname, "./error.html")

//funcion normal y llamando la linea
server.get("/", devolverIndex)

//funcion flecha
server.get("/about",(req,res)=>{
    res.write(about)
})

server.use(express.json())

server.listen(puerto,()=>{
    console.log(`El servidor esta ejecutansode en el puerto ${puerto}`)
})

//definir un middleware muy basico
server.use((req,res,next)=>{
    res.status(404).sendFile(paginaError)
})

function devolverIndex(req,res)
{
    res.write(index)
}

/*const { response } = require('express')
const fs= require('fs')
const http= require('http')

const index = fs.readFile('.\file.html');
const about= fs.readFile('.\about.html');


http.createServer((request, response)=>{
    const{url}= request;

    if(url==='/')
    {
        response.writeHead(200,{"content-tipe":"text/html"});
        response.write(index);
    }
    else if(url ==='/about')
    {
        response.writeHead(200,{"content-tipe":"text/html"});
        response.write(about);
    }
    else
    {
        response.writeHead(404,{"content-tipe":"text/html"});
        response.write("no se encuentra");
    }


    console.log(url);
})

.listen(3000, ()=>{
    console.log('Servidor correcto')
})
*/