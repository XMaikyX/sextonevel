//definir una persistencia(Arreglo, base de datos, servicios web monitoreo)

const libros =[
    {
        id:1,
        titulo:'AMerican Pie',
        idautor:2
    },
    {
        id:2,
        titulo:'El socio',
        idautor:2
    },
    {
        id:3,
        titulo:' 30 dias de muerte',
        idautor:2
    }
]

const autores =[
    {
        id:1,
        nombre:'El pepe'
    },
    {
        id:2,
        nombre:' Stalone'
    },
    {
        id:3,
        nombre:'Luisillo Pillo'
    }
]


//APLICAR PROMESAS

//funcion buscar libro ID
function buscarLibro(id)
{
    return new Promise ((resolve, reject) =>{

        // se busca en el arreglo que tenga un id
        const libro=libros.find(
            (libro)=>
            {
                return libro.id=== id;
            }
        )
        // caso contrario envia error regresando al callback
        if ( !libro)
        {
            const error =new Error();
            error.message="No hay esa salvajada"
            reject (error);
        }
        //si no existe error debemos enviar el libro al callback y aceptamos la promesa
        resolve(libro);
    })
    // buscamos en el arreglo de libros con un id en especifico
}

//buscar autor id
function buscarAutor(id)
{
    return new Promise((resolve,reject)=>{
        
        // buscamos en el arreglo de autor con un id en especifico
        const autor=autores.find(
            (autor)=>{
                return autor.id=== id;
            }
        )
        // caso contrario envia error regresando al callback
        if ( !autor)
        {
            const error =new Error();
            error.message="No hay ese emprendedor"
            reject(error);
        }
        //si no existe error debemos enviar el autor al callback
        resolve(autor);
    })
}


//buscar libro ID
buscarLibro(3).then(libro=>{
    libroAux=libro;
    //console.log(libro);
    return buscarAutor(libro.idautor);
}).then(autor=>{
    libroAux.autor=autor;
    delete libroAux.idautor;
    console.log(libroAux);
}).catch(err=>{
    console.log(err.message)
})

