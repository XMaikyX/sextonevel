//definir una persistencia(Arreglo, base de datos, servicios web monitoreo)

const libros =[
    {
        id:1,
        titulo:'AMerican Pie',
        idautor:2

    },
    {
        id:2,
        titulo:'El socio',
        idautor:3
    },
    {
        id:3,
        titulo:' 30 dias de muerte',
        idautor:2
    }
]

const autores =[
    {
        id:1,
        nombre:'El pepe'
    },
    {
        id:2,
        nombre:' Stalone'
    },
    {
        id:3,
        nombre:'Luisillo Pillo'
    }
]
function buscarLibro(id, callback)
{
    // buscamos en el arreglo de libros con un id en especifico
    const libro=libros.find(
        (libro)=>{
            return libro.id=== id;
        }
    )
    // caso contrario envia error regresando al callback
    if ( !libro)
    {
        const error =new Error();
        error.message="No hay esa salvajada"
        return callback(error);
    }
    //si no existe error debemos enviar el libro al callback
    callback(null, libro);
}



function buscarAutor(id, callback)
{
    // buscamos en el arreglo de autor con un id en especifico
    const autor=autores.find(
        (autor)=>{
            return autor.id=== id;
        }
    )
    // caso contrario envia error regresando al callback
    if ( !autor)
    {
        const error =new Error();
        error.message="No hay ese emprendedor"
        return callback(error);
    }
    //si no existe error debemos enviar el autor al callback
    callback(null, autor);
}

//CALLBACK HELL

//se busca un callback  para tener el id del libro
buscarLibro(2,(err,libro)=>{
    if(err)
    {
        return console.log(err.message)
    }

    buscarAutor(libro.idautor,(err,autor)=>{
        if (err)
        {
            return console.log(err.message);
        }
        //console.log(`El libro ${libro.titulo} fue escrito por ${autor.nombre}`);
        libro.autor=autor
        delete libro.idautor;
        console.log(libro)
    })
    //return console.log(libro);

}) 
