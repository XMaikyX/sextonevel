const container = require('./src/startup/container')

const server = container.resolve("app")

const {Mongo_URI} = container.resolve("config");

const mongoose = require ('mongoose');
mongoose.set('useCreateIndex',true);


mongoose.connect(Mongo_URI, 
    {
        useNewUrlParser:true ,
        useFindAndModify:true, 
        useUnifiedTopology:true
    }).then(()=>{
        server.start();
    }).catch(console.log)