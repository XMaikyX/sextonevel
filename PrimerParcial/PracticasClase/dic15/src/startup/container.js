const {createContainer,asClass,asFunction,asValue}=require(`awilix`);

//config
const config= require('../config')

//startup
const app= require('')

//services

const {HomeService}= require('../services')

//controlers
const{HomeController} =require('../controllers')

//routers
const {HomeRoutes}= require('../routes/index.routes');
const Routes = require('../routes');
const routes = require('../routes');



const container=createContainer();

container.register=
(
    {
        app: asClass(app).singleton(),
        router: asFunction(Routes).singleton(),
        config: asValue(config),
    }
).register(
    {
        HomeService:asClass(HomeService).singleton()
    }
).register(
    {
        HomeController: asClass(HomeController.bind(HomeController)).singleton()
    }
).register(
    {
        HomeRoutes: asFunction(HomeRoutes).singleton()
    }
)

module.exports= container;