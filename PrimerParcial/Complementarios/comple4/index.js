const express = require ('express');
const app = express();
const PORT= 3000

let arreglo=[];

app.use('/public', express.static(__dirname+'/public'))
app.use(express.json())

app.get('/',(req,res)=>{
    res.send(arreglo)
})

app.listen((PORT),()=>{
    console.log(`El servidor se ejecuta en el puerto ${PORT}`)
})

app.get('/individual/:nombres', (req,res)=>{
    res.send( arreglo.filter(p=>{return p.nombre== req.params.nombres; })[0])
 })