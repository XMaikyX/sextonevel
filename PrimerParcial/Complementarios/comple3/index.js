const mongoose = require('mongoose');
//librerias instaladas
const axios= require ('axios').default;
const cheerio = require('cheerio')
const cron= require('node-cron')
//Componentes Locales
const {MONGO_URI}= require ('./config')
const {Informa} = require ('./models')


//conectarnos a nuestra base de datos ORM
mongoose.connect(MONGO_URI, {useNewUrlParser:true, useUnifiedTopology:true})
.then(p =>{
    console.log('Esta conectado')
}).catch(error=>{
    console.log('no vale '+err);
})

cron.schedule("0 3 * * *", async ()=>{
    

    //acceder a nuestra URL de CNN para tomar el html con AXIOS
    const html = await axios.get("https://vandal.elespanol.com/noticias/videojuegos");


    //procesar  ese HTML  utilizando  cheerio y recorrer las noticias
    const $= cheerio.load(html.data);

    
    //llamar a los parametros que pertenecen a la clase  new_title
    const titulos = $(".titulocaja");
    titulos.each((index,element)=>{

        const informa = {
            titulo: $(element).text().toString(),
            enlace: $(element).children().attr("href")
        }
        Informa.create([informa]);
    })

    console.log('proceso terminado')
})
