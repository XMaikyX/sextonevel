const reservacion =[
    {
        id:1,
        fecha:'22/12/2020',
        hora:'21:30',
        idusuario:2
    },
    {
        id:2,
        fecha:'11/12/2020',
        hora:'20:00',
        idusuario:3
    },
    {
        id:3,
        fecha:'15/12/2020',
        hora:'20:30',
        idusuario:1
    }
]

const usuario =[
    {
        id:1,
        nombre:'Zeus',
        idmesa:3
    },
    {
        id:2,
        nombre:'Poseidon',
        idmesa:1
    },
    {
        id:3,
        nombre:'Deimos',
        idmesa:2
    }
]
const mesa=[
    {
        id:1,
        tipo:'pareja',
        cantidad:'2'
    },
    {
        id:2,
        tipo:'falimiar',
        cantidad:'6'
    },
    {
        id:3,
        tipo:'zona VIP',
        cantidad:'4'
    },
    
]


//PROMESAS
/*
function buscarReservacion(id)
{
    return new Promise ((resolve, reject) =>{

        
        const reserva=reservacion.find(
            (reserva)=>
            {
                return reserva.id=== id;
            }
        )
        
        if ( !reserva)
        {
            const error =new Error();
            error.message="No hay esa reservacion"
            reject (error);
        }
        
        resolve(reserva);
    })
    
}

//buscar USUARIO id
function buscarUsuario(id)
{
    return new Promise((resolve,reject)=>{
        
        
        const usuari=usuario.find(
            (usuari)=>{
                return usuari.id=== id;
            }
        )

        if ( !usuari)
        {
            const error =new Error();
            error.message="No hay registro con ese nombre"
            reject(error);
        }

        resolve(usuari);
    })
}
function buscarMesa(id)
{
    return new Promise((resolve,reject)=>{
        
        const messie=mesa.find(
            (messie)=>{
                return messie.id=== id;
            }
        )
        if ( !messie)
        {
            const error =new Error();
            error.message="No existe mesa"
            reject(error);
        }
        resolve(messie);
    })
}
*/
/*
buscarReservacion(3).then(reservacion=>{
    reservas=reservacion;
    return buscarUsuario(reservacion.idusuario);
}).then(usuario=>{
    reservas.usuario=usuario;
    return buscarMesa(usuario.idmesa);
}).then(mesa=>{
    reservas.usuario.mesa=mesa;
    delete reservas.usuario.idmesa;
    console.log(reservas);
}).catch(err=>{
    console.log(err.message)
})*/


//ASYNC
async function buscarReservacion(id)
{
    // se busca en el arreglo que tenga un id
    const reserva=reservacion.find(
        (reserva)=>
        {   
            return reserva.id=== id;
        }
    )
    // caso contrario envia error y lo lanzamos
    if ( !reserva)
    {
        const error =new Error();
        error.message="No hay esa reservacion"
        throw error;
    }
    //si no existe libro lo regresamos
    return reserva;

}

async function buscarUsuario(id)
{
    // se busca en el arreglo que tenga un id
    const user=usuario.find(
        (user)=>
        {   
            return user.id=== id;
        }
    )
    // caso contrario envia error y lo lanzamos
    if ( !user)
    {
        const error =new Error();
        error.message="No hay ese usuario en el registro"
        throw error;
    }
    //si no existe libro lo regresamos
    return user;

}
async function buscarMesa(id)
{
    // se busca en el arreglo que tenga un id
    const mesas=mesa.find(
        (mesas)=>
        {   
            return mesas.id=== id;
        }
    )
    // caso contrario envia error y lo lanzamos
    if ( !mesas)
    {
        const error =new Error();
        error.message="No hay ese tipo de mesa"
        throw error;
    }
    //si no existe libro lo regresamos
    return mesas;

}



async function mainx()
{
    try
    {
        //busqueda de id de libro
        const reservacion = await buscarReservacion(3);
        //empieza a buscar su respectivo autor
        const usuario = await buscarUsuario(reservacion.idusuario);

        const  mesa= await buscarMesa(usuario.idmesa);

        reservacion.usuario=usuario;
        reservacion.usuario.mesa=mesa;
        delete reservacion.idusuario;
        console.log(reservacion)
        //console.log(`El libro ${libro.titulo} tiene como autor A ${autor.nombre}`);
    }
    catch(second)
    {
        console.log(second.message);
    }
}


mainx()